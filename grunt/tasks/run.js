module.exports = function (grunt) {
    return function (target) {
        return grunt.task.run([
            'injector',
            'connect:livereload',
            'watch'
        ]);
    };
};