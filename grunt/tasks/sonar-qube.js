module.exports = function (grunt) {
    return {
        task: function () {
            var os     = require('os'),
                fs     = require('fs'),
                xml2js = require('xml2js');

            var parser = new xml2js.Parser();
            var appName = 'Registry Hop';
            var appVersion = '1.0.0';
            var appId = 'com.registryhop';

            var options = this.options({
                debug    : false,
                separator: os.EOL, // jshint ignore: line
                dryRun   : false
            });
            var mergeOptions = function (prefix, effectiveOptions, obj) {
                obj.projectKey = appId;
                obj.projectName = appName;
                obj.projectVersion = appVersion;

                for (var j in obj) {
                    if (typeof obj[j] === 'object') {
                        mergeOptions(prefix + j + '.', effectiveOptions, obj[j]);
                    }
                    else {
                        effectiveOptions[prefix + j] = obj[j];
                    }
                }
            };
            var effectiveOptions = Object.create(null);
            mergeOptions('sonar.', effectiveOptions, options.sonar);
            var props = [];
            if (options.debug) {
                grunt.log.writeln('Effective Sonar Options');
                grunt.log.writeln('-----------------------');
            }
            for (var o in effectiveOptions) {
                var line = o + '=' + effectiveOptions[o];
                props.push(line);
                if (options.debug) {
                    grunt.log.writeln(line);
                }
            }

            grunt.file.write('sonar-project.properties', props.join(options.separator));
        }
    };
};