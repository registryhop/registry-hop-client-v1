module.exports = function (grunt, app) {
    return {
        bower: {
            files: ['bower.json'],
            tasks: ['injector']
        },

        sassConcat: {
            files: require('./concat.js').sass.src,
            tasks: ['concat:sass']
        },

        sassStyles: {
            files: ['<%= app.src %>/scss/app.scss'],
            tasks: ['sass']
        },

        gruntfile: {
            files: ['gruntfile.js']
        },

        livereload: {
            options: {
                livereload: '<%= connect.options.livereload %>'
            },
            files  : [
                '<%= app.src %>/views/index.html'
            ].concat(app.sources),
            tasks  : ['injector']
        }
    };
};