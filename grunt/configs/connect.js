module.exports = {
    options   : {
        port      : 9000,
        hostname  : 'localhost',
        livereload: 35729
    },
    livereload: {
        options: {
            open     : true,
            directory: '<%= app.src %>',
            base     : {
                path   : '<%= app.src %>',
                options: {
                    index : 'views/index.html',
                    maxAge: 10000
                }
            }
        }
    }
};