module.exports = {
    dist      : {
        files: [{
            expand: true,
            dot   : true,
            cwd   : '<%= app.src %>',
            dest  : '<%= app.dest %>',
            src   : [
                '*.{ico,png,txt}',
                '.htaccess',
                'index.html',
                '/views/*.html',
                '**/views/*.html',
                '**/**/views/*.html',
                '**/**/**/views/*.html',
                '**/**/**/**/views/*.html',
                '**/**/**/**/**/views/*.html'
            ]
        }, {
            expand: true,
            cwd   : '.tmp/images',
            dest  : '<%= app.dest %>/img',
            src   : ['generated/*']
        }]
    },
    fontGlyphs: {
        expand: true,
        cwd   : '/bower_components/components-font-awesome/fonts',
        dest  : '<%= app.dest %>/fonts',
        src   : ['!*.txt', '*.*']
    }
};