module.exports = function (grunt, app) {
    grunt.log.verbose.error(app.sources);
    var cfg = {
        options          : {
            addRootSlash: false,
            ignorePath  : [
                '<%= app.src %>'
            ],
            bowerPrefix : 'bower'
        },
        localDependencies: {
            files: {
                '<%= app.src %>/views/index.html': app.sources
            }
        },
        bowerDependencies: {
            files: {
                '<%= app.src %>/views/index.html': ['bower.json']
            }
        },
        karmaDependencies: {
            options: {
                ignorePath: '',
                transform : function (filepath) {
                    return '\'' + filepath + '\',';
                }
            },
            files  : {
                'karma.conf.js': ['bower.json'].concat(app.sources)
            }
        }
    };
    return cfg;
};