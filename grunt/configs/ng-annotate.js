module.exports = {
    options: {
        singleQuotes: true
    },
    dist   : {
        files: [{
            expand: true,
            cwd   : '<%= app.tmp %>/concat/scripts',
            src   : '*.js',
            dest  : '<%= app.tmp %>/concat/scripts'
        }]
    }
};