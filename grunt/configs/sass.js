module.exports = {
    options: {
        includePaths: [
            '<%= app.src %>/lib/foundation/scss/',
        ]
    },

    dist: {
        options: {
            outputStyle: 'compressed'
        },
        files  : {
            '<%= app.dest %>/css/app.css': '<%= app.src %>/scss/app.scss'
        }
    }
};