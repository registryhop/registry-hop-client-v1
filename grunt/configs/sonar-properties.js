module.exports = function (grunt) {
    return {
        buildFile: {
            options: {
                debug    : true,
                separator: '\n',
                dryRun   : false,
                sonar    : {
                    sources: [
                        '<%= app.src %>/controllers/*.js',
                        '<%= app.src %>/**/controllers/*.js',
                        '<%= app.src %>/**/**/controllers/*.js',
                        '<%= app.src %>/**/**/**/controllers/*.js',
                        '<%= app.src %>/**/**/**/**/controllers/*.js',
                        '<%= app.src %>/**/**/**/**/**/controllers/*.js',
                        '<%= app.src %>/directives/*.js',
                        '<%= app.src %>/**/directives/*.js',
                        '<%= app.src %>/**/**/directives/*.js',
                        '<%= app.src %>/**/**/**/directives/*.js',
                        '<%= app.src %>/**/**/**/**/directives/*.js',
                        '<%= app.src %>/**/**/**/**/**/directives/*.js',
                        '<%= app.src %>/filters/*.js',
                        '<%= app.src %>/**/filters/*.js',
                        '<%= app.src %>/**/**/filters/*.js',
                        '<%= app.src %>/**/**/**/filters/*.js',
                        '<%= app.src %>/**/**/**/**/filters/*.js',
                        '<%= app.src %>/**/**/**/**/**/filters/*.js',
                        '<%= app.src %>/services/*.js',
                        '<%= app.src %>/**/services/*.js',
                        '<%= app.src %>/**/**/services/*.js',
                        '<%= app.src %>/**/**/**/services/*.js',
                        '<%= app.src %>/**/**/**/**/services/*.js',
                        '<%= app.src %>/**/**/**/**/**/services/*.js'
                    ],
                    tests  : [
                        '<%= app.src %>/tests/*.js',
                        '<%= app.src %>/**/tests/*.js',
                        '<%= app.src %>/**/**/tests/*.js',
                        '<%= app.src %>/**/**/**/tests/*.js',
                        '<%= app.src %>/**/**/**/**/tests/*.js',
                        '<%= app.src %>/**/**/**/**/**/tests/*.js'
                    ]
                }
            }
        }
    };
};