module.exports = {
    sass: {
        src : [
            '<%= app.src %>/scss/settings.scss',
            '<%= app.src %>/scss/custom-settings.scss',
            '<%= app.src %>/scss/foundation.scss',
            '<%= app.src %>/**/**/**/**/**/*.scss',
            '<%= app.src %>/**/**/**/**/*.scss',
            '<%= app.src %>/**/**/**/*.scss',
            '<%= app.src %>/**/**/*.scss',
            '<%= app.src %>/**/*.scss',
        ],
        dest: '<%= app.src %>/scss/app.scss'
    }
};