module.exports = (function () {
    var app = {
        src : 'application',
        dest: 'release',
        tmp : '.tmp'
    };
    var sources = [
        app.src + '/*.module.js',
        app.src + '/**/*.module.js',
        app.src + '/**/**/*.module.js',
        app.src + '/**/**/**/*.module.js',
        app.src + '/**/**/**/**/*.module.js',
        app.src + '/**/**/**/**/**/*.module.js',
        app.src + '/config/*.js',
        app.src + '/**/config/*.js',
        app.src + '/**/**/config/*.js',
        app.src + '/**/**/**/config/*.js',
        app.src + '/**/**/**/**/config/*.js',
        app.src + '/**/**/**/**/**/config/*.js',
        app.src + '/directives/*.js',
        app.src + '/**/directives/*.js',
        app.src + '/**/**/directives/*.js',
        app.src + '/**/**/**/directives/*.js',
        app.src + '/**/**/**/**/directives/*.js',
        app.src + '/**/**/**/**/**/directives/*.js',
        app.src + '/filters/*.js',
        app.src + '/**/filters/*.js',
        app.src + '/**/**/filters/*.js',
        app.src + '/**/**/**/filters/*.js',
        app.src + '/**/**/**/**/filters/*.js',
        app.src + '/**/**/**/**/**/filters/*.js',
        app.src + '/services/*.js',
        app.src + '/**/services/*.js',
        app.src + '/**/**/services/*.js',
        app.src + '/**/**/**/services/*.js',
        app.src + '/**/**/**/**/services/*.js',
        app.src + '/**/**/**/**/**/services/*.js',
        app.src + '/controllers/*.js',
        app.src + '/**/controllers/*.js',
        app.src + '/**/**/controllers/*.js',
        app.src + '/**/**/**/controllers/*.js',
        app.src + '/**/**/**/**/controllers/*.js',
        app.src + '/**/**/**/**/**/controllers/*.js'
    ];

    return {
        src    : app.src,
        dest   : app.dest,
        tmp    : app.tmp,
        sources: sources
    };
})();