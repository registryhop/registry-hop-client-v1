module.exports = function (grunt) {
    'use strict';

    require('load-grunt-tasks')(grunt);

    var app = require('./grunt/configs/app.js');
    var config = {
        app            : app,
        concat         : require('./grunt/configs/concat.js'),
        connect        : require('./grunt/configs/connect.js'),
        copy           : require('./grunt/configs/copy.js'),
        injector       : require('./grunt/configs/injector.js')(grunt, app),
        karma          : require('./grunt/configs/karma.js'),
        ngAnnotate     : require('./grunt/configs/ng-annotate.js'),
        sass           : require('./grunt/configs/sass.js'),
        sonarProperties: require('./grunt/configs/sonar-properties.js')(grunt),
        watch          : require('./grunt/configs/watch.js')(grunt, app)
    };

    grunt.initConfig(config);

    /**
     * Tasks
     */
    var Tasks = {
        sonarQube: require('./grunt/tasks/sonar-qube.js'),
        run      : require('./grunt/tasks/run.js')
    };

    grunt.registerMultiTask('sonarProperties',
            'Create Sonar properties from grunt', Tasks.sonarQube(grunt).task);
    grunt.registerTask('default', Tasks.run(grunt));
};