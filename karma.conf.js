module.exports = function (config) {
    'use strict';

    config.set({
        // base path, that will be used to resolve files and exclude
        basePath: '',

        // Frameworks to use
        frameworks: ['jasmine'],

        // 'ngResource', 'ngCookies', 'ngAnimate', 'ngTouch', 'ngSanitize', 'ui.router', 'ui.bootstrap', 'ui.utils'

        // List of files / patterns to load in the browser
        files: [
            <!-- injector:bowerjs -->
            'application/lib/angular/angular.js',
            'application/lib/angular-animate/angular-animate.js',
            'application/lib/angular-sanitize/angular-sanitize.js',
            'application/lib/angular-ui-router/release/angular-ui-router.js',
            'application/lib/ionic/js/ionic.js',
            'application/lib/ionic/js/ionic-angular.js',
            'application/lib/angular-mocks/angular-mocks.js',
            <!-- endinjector -->

            <!-- injector:js -->
            'application/application.module.js',
            'application/accounts/accounts.module.js',
            'application/accounts/signup/accounts.signup.module.js',
            'application/accounts/signup/config/config.js',
            'application/accounts/signup/controllers/sign-up.js',
            <!-- endinjector -->
        ],

        preprocessors: {
            'app/modules/*/views/directive-*.html': 'html2js'
        },

        // Test results reporter to use
        // Possible values: 'dots', 'progress', 'junit', 'growl', 'coverage'
        //reporters: ['progress'],
        reporters: ['progress', 'junit'],

        junitReporter: {
            outputFile: 'test-results.xml',
            suite     : ''
        },

        // Web server port
        port: 9876,

        // Enable / disable colors in the output (reporters and logs)
        colors: true,

        // Level of logging
        // Possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO ||
        // config.LOG_DEBUG
        logLevel: config.LOG_INFO,

        // Enable / disable watching file and executing tests whenever any file changes
        autoWatch: true,

        // Start these browsers, currently available:
        // - Chrome
        // - ChromeCanary
        // - Firefox
        // - Opera
        // - Safari (only Mac)
        // - PhantomJS
        // - IE (only Windows)
        browsers: ['PhantomJS'],

        // If browser does not capture in given timeout [ms], kill it
        captureTimeout: 60000,

        browserNoActivityTimeout: 100000,

        // Continuous Integration mode
        // If true, it capture browsers, run tests and exit
        singleRun: true
    });
};