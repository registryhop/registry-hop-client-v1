(function () {
    'use strict';

    angular
        .module('accounts.signup')
        .config(AccountsSignupRoutes);

    /* @ngInject */
    function AccountsSignupRoutes($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/accounts/signup');

        $stateProvider
            .state('accounts-signup', {
                url         : '/accounts/signup',
                templateUrl : 'accounts/signup/views/index.html',
                controller  : 'SignUp',
                controllerAs: 'signUp'
            });
    }
})();