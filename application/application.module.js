(function () {
    'use strict';

    var app = 'registryHop';
    var dependencies = [
        'ionic',
        'ui.router',
        'accounts'
    ];

    angular
        .module(app, dependencies)
        .config(config);

    angular
        .element(document)
        .ready(bootstrap);

    /* @ngInject */
    function config($locationProvider) {
        $locationProvider.hashPrefix('!');
    }

    function bootstrap() {
        angular
            .bootstrap(document, [app]);
    }
})();

